# Print the plastic parts

If you have bought the plastic parts you can skip this step!

{{BOM}}

[PLA filament]: parts/materials/pla_filament.md "{cat:material}"
[Black PLA filament]: parts/materials/black_pla_filament.md "{cat:material}"
[RepRap-style printer]: parts/tools/rep-rap.md
[utility knife]: parts/tools/utility-knife.md
[custom print settings]: smart_brim.md

## Printing {pagestep}

Now you have tested your [printer][RepRap-style printer]{qty:1,cat:tool} and [filament][PLA filament]{Qty: 200g, note:"Of any colour you want. Two contrasting colours may look best."}. you can print the following parts:

* Actuator assembly tools ([nut tool]{output,qty:1}, [band tool]{output,qty:1}, and [band tool cover]{output,qty:1}):  [actuator_assembly_tools.stl](models/actuator_assembly_tools.stl){previewpage}
* [Lens tool]{output,qty:1}: [lens_tool.stl](models/lens_tool.stl){previewpage}
* [Main body]{output,qty:1}: [main_body.stl](models/main_body.stl){previewpage} - The smart brim may require [custom print settings].
* 3 [feet]{output,qty:3}: [feet.stl](models/feet.stl){previewpage}
* 3 [large gears]{output,qty:3}: [large_gears.stl](models/large_gears.stl){previewpage}
* 3 [cable tidy caps]{output,qty:3}: [cable_tidies.stl](models/cable_tidies.stl){previewpage}
* [Illumination dovetail]{output,qty:1}: [illumination_dovetail.stl](models/illumination_dovetail.stl){previewpage}
* [Condenser arm]{output,qty:1}: [condenser.stl](models/condenser.stl){previewpage}
* [Illumination thumbscrew]{output,qty:1}: [illumination_thumbscrew.stl](models/illumination_thumbscrew.stl){previewpage}
* [Sample clips]{output,qty:2}: [sample_clips.stl](models/sample_clips.stl){previewpage}
* [Optics module]{output,qty:1}: [optics_picamera_2_rms_f50d13.stl](models/optics_picamera_2_rms_f50d13.stl){previewpage} - **This must be printed in [black][Black PLA filament]{Qty: 50g}!** ([?](why_optics_black.md "why?"))
* [pi camera cover]{output,qty:1}: [picamera_2_cover.stl](models/picamera_2_cover.stl){previewpage}
* [Microscope stand]{output, qty:1}: [microscope_stand.stl](models/microscope_stand.stl){previewpage}
* 3 [small gears]{output,qty:3}: [small_gears.stl](models/small_gears.stl){previewpage}
* [Base to hold the motor driver]{output,qty:1}: [motor_driver_case_sangaboard.stl](models/motor_driver_case_sangaboard.stl){previewpage}



## Clean-up of printed parts {pagestep}

Carefully remove the printing brim from all parts (except the main body) with a [utility knife]{qty: 1, cat: tool}.